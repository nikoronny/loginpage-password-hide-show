import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  email: string;
  password: string;
  constructor(public navCtrl: NavController) {

  }
  show = () => {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
}
